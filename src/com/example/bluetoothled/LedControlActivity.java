package com.example.bluetoothled;

import java.io.IOException;
import java.util.UUID;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class LedControlActivity extends ActionBarActivity {

	
	Button button1, button2;
	SeekBar sb1,sb2;
	String address = null;
	private ProgressDialog progress;
	BluetoothAdapter myBluetooth = null;
	BluetoothSocket btSocket = null;
	private boolean isBtConnected = false;
	static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_led_control);
		
		
		initializeAll();
		
		
		Intent newIntent = getIntent();
		address = newIntent.getStringExtra(MainActivity.EXTRA_ADDRESS);
		
		button1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toggleLED1();
			}

			
		});
		
		button2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toggleLED2();
			}
		});
		
		sb1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				if (fromUser == true) {
					try
		            {
						
		                btSocket.getOutputStream().write(String.valueOf(progress).getBytes());
		            }
		            catch (IOException e)
		            {

		            }
				}
			}
		});
		
		sb2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				if (fromUser == true) {
					try
		            {
		                btSocket.getOutputStream().write(String.valueOf(progress).getBytes());
		            }
		            catch (IOException e)
		            {

		            }
				}
			}
		});
	}

	private void initializeAll() {
		// TODO Auto-generated method stub
		button1 = (Button) findViewById(R.id.ledButton1);
		button2 = (Button) findViewById(R.id.ledButton2);
		sb1 = (SeekBar) findViewById(R.id.seekBar1);
		sb2 = (SeekBar) findViewById(R.id.seekBar2);
	}

	private void toggleLED1() {
		// TODO Auto-generated method stub
		 if (btSocket!=null)
		    {
		        try
		        {
		            btSocket.getOutputStream().write("TOGGLE1".toString().getBytes());
		        }
		        catch (IOException e)
		        {
		        	Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
		        }
		    }
	}
	
	private void toggleLED2() {
		// TODO Auto-generated method stub
		 if (btSocket!=null)
		    {
		        try
		        {
		            btSocket.getOutputStream().write("TOGGLE2".toString().getBytes());
		        }
		        catch (IOException e)
		        {
		        	Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
		        }
		    }
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.led_control, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class ConnectBT extends AsyncTask<Void, Void, Void>  
	{
	    private boolean ConnectSuccess = true; 

	    @Override
	    protected void onPreExecute()
	    {
	        progress = ProgressDialog.show(LedControlActivity.this, "Connecting...", "Please wait!!!");  
	    }

	    @Override
	    protected Void doInBackground(Void... devices) 
	    {
	        try
	        {
	            if (btSocket == null || !isBtConnected)
	            {
	             myBluetooth = BluetoothAdapter.getDefaultAdapter();
	             BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);
	             btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);
	             BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
	             btSocket.connect();//start connection
	            }
	        }
	        catch (IOException e)
	        {
	            ConnectSuccess = false;//if the try failed, you can check the exception here
	        }
	        return null;
	    }
	    @Override
	    protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
	    {
	        super.onPostExecute(result);

	        if (!ConnectSuccess)
	        {
	        	Toast.makeText(getApplicationContext(), "Connection Failed", Toast.LENGTH_LONG).show();
	        	
	            finish();
	        }
	        else
	        {
	        	Toast.makeText(getApplicationContext(), "Bluetooth Module Connected", Toast.LENGTH_LONG).show();
	            isBtConnected = true;
	        }
	        progress.dismiss();
	    }
	}
}
