package com.example.bluetoothled;

import java.util.ArrayList;
import java.util.Set;

import com.example.bluetoothled.R.id;

import android.support.v7.app.ActionBarActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

	Button btnPaired;
	ListView devicelist;
	private BluetoothAdapter myBluetooth = null;
	 private Set<BluetoothDevice> pairedDevices;
	ArrayList list;
	public static final String EXTRA_ADDRESS = "address";
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        initialize();
        
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null)
           {
             
             Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
           
             finish();
           }
          else
           
        	  if (!myBluetooth.isEnabled())
            {
        	
        	Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        	startActivityForResult(turnBTon,1);
            }
        
        btnPaired.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pairedDevicesList();
			}

			
		});
        
        
    }


    private void initialize() {
		// TODO Auto-generated method stub
    	btnPaired = (Button) findViewById(R.id.pairDevicesButton);
    	devicelist = (ListView) findViewById(R.id.pairedDevicesLV);
    	list = new ArrayList();
	}
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (resultCode == RESULT_CANCELED) {
    		Toast.makeText(getApplicationContext(), "Turn on Bluetooth to Continue", Toast.LENGTH_SHORT).show();
			finish();
		}
    	
    }
    private void pairedDevicesList() {
		// TODO Auto-generated method stub
		pairedDevices = myBluetooth.getBondedDevices();
		if (pairedDevices.size()>0)
	    {
	        for(BluetoothDevice bt : pairedDevices )
	        {
	            list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
	        }
	    }
	    else
	    {
	        Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
	    }
		final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
	    devicelist.setAdapter(adapter);
	    devicelist.setOnItemClickListener(myListClickListener);
	}

private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener(){

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		String info = ((TextView) view).getText().toString();
		String address = info.substring(info.length() - 17);
		Intent i = new Intent(MainActivity.this, LedControlActivity.class );
		i.putExtra(EXTRA_ADDRESS, address);
		startActivity(i);
	}
	
	
};
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
